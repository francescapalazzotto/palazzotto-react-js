import './App.css'
import NavigationBar from './components/NavigationBar/NavigationBar'
import Carousel from './components/Carousel/Carousel'
import Information from './components/Information/Information'
import Cards from './components/Cards/Cards'
import InfoTour from './components/TourDates/InfoTour'
import TourDates from './components/TourDates/TourDates'
import ContactsForm from './components/ContactsForm/ContactsForm'

function App() {


  return (
    <>
      {/* Barra di navigazione all'interno dell'header */}
      <header>
        <NavigationBar home="Home" name1="Band" name2="Tour" name3="Contact" name4="More"></NavigationBar>
      </header>

      {/* Carosello degli album */}
      <Carousel />

      {/* Informazioni band */}
      <div className="information">
        <Information name="NIRVANA" />
        <Cards />
      </div>

      {/* Tour dates */}
      <div className="tour-dates">
        <InfoTour />
        <TourDates />
      </div>

      {/* Contatti e form di registrazione */}
      <div className="contatti">
        <ContactsForm />
      </div>

      <footer>
        <p>
            Designed and built with all the love in the world by the 
            Nirvana team. <br />
            Code licensed ABC. <br />
            All rights reserved - Nirvana 1987
        </p>
      </footer>

    </>
  )
}

export default App
