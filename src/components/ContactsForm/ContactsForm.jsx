export default function ContactsForm(){
    return(
        <>
            <h1 id="contact" style={{color: "black"}}>Contacts</h1>
            
            <div className="container">
                <div className="row">
                    <div className="col-md-4">
                        <p className="contatti-desc">Fan? Drop a note. <br />
                            Chicago, US. <br />
                            Phone: +00 1515151515 <br />
                            Email: mail@mail.com 
                        </p>

                    </div>

                    <div className="col-md-8">
                        <input type="text" value="Name" />
                        <input type="text" value="Email" /> <br />
                        <textarea name="comment" id="comment" cols="45" rows="5" style={{position: "relative"}}>
                            Comment
                        </textarea> <br />
                        <a href="https://mail.google.com/mail/" className="btn btn-dark" style={{color: "white"}}>Send</a>
                    </div>

                </div>
            </div>
        </>
    )
}