export default function Cards(){
    return(
        <>
        <div className="container">
            <div className="row">
                
                <div className="col-md-4">
                    <div className="card">
                        <img src="https://i.pinimg.com/736x/04/72/83/047283209bf31ac811982d4b55aa965d.jpg" className="card-img-top" alt="Dave Grohl Image"></img>
                        <div className="card-body">
                          <h5 className="card-title"> <b>David Eric Grohl</b></h5>
                          <p className="card-text" style={{fontStyle: "italic"}}>
                            Born January 14, 1969, is an American musician. 
                            He is the founder of the rock band Foo Fighters, 
                            for which he is the lead singer, guitarist, 
                            and principal songwriter. Prior to forming 
                            Foo Fighters, he was the drummer of the rock 
                            band Nirvana from 1990 to 1994.
                          </p>
                          <a href="https://en.wikipedia.org/wiki/Dave_Grohl#Nirvana_(1990%E2%80%931994)" className="btn btn-primary" style={{color: "white"}}>More details</a>
                        </div>
                    </div>
                </div>

                <div className="col-md-4">
                    <div className="card">
                        <img src="https://hips.hearstapps.com/hmg-prod/images/kurt-cobain-vita-morte-1651567577.jpg?crop=0.676xw:1.00xh;0.0813xw,0&resize=1200:*" className="card-img-top" alt="Kurt Cobain Image"></img>
                        <div className="card-body">
                          <h5 className="card-title"> <b>Kurt Donald Cobain</b></h5>
                          <p className="card-text" style={{fontStyle: "italic"}}>
                            Born February 20, 1967, is n American musician 
                            who was the lead vocalist, guitarist, 
                            primary songwriter, and a founding member of the 
                            rock band Nirvana. Through his angst-fueled 
                            songwriting and anti-establishment persona, 
                            his compositions widened the thematic conventions 
                            of mainstream rock music. He was heralded as a 
                            spokesman of Generation X and is widely recognized
                            as one of the most influential alternative rock musicians. 
                          </p>
                          <a href="https://en.wikipedia.org/wiki/Kurt_Cobain" className="btn btn-primary" style={{color: "white"}}>More details</a>
                        </div>
                    </div>
                </div>

                <div className="col-md-4">
                    <div className="card">
                        <img src="https://i.pinimg.com/564x/c4/3b/68/c43b6871d8ad1e9ae959434a3c39151f.jpg" className="card-img-top" alt="Kris Novoselic Image"></img>
                        <div className="card-body">
                          <h5 className="card-title"><b>Krist Anthony Novoselic</b></h5>
                          <p className="card-text" style={{fontStyle: "italic"}}>
                            Born May 16, 1965, is an American musician 
                            and activist. Novoselic co-founded the rock band 
                            Nirvana and played bass for the band.
                          </p>
                          <a href="https://en.wikipedia.org/wiki/Krist_Novoselic" className="btn btn-primary" style={{color: "white"}}>More details</a>
                        </div>
                    </div>
                </div>

            </div>
        </div> 
        </>
    )
}