import InUtero from "../../assets/InUteroAlbumCover.jpg"

export default function Carousel(){
    return(
        <>
            <div id="carouselExampleCaptions" className="carousel slide">
                <div className="carousel-indicators">
                <button type="button" data-bs-target="#carouselExampleCaptions" data-bs-slide-to="0" className="active" aria-current="true" aria-label="Slide 1"></button>
                <button type="button" data-bs-target="#carouselExampleCaptions" data-bs-slide-to="1" aria-label="Slide 2"></button>
                <button type="button" data-bs-target="#carouselExampleCaptions" data-bs-slide-to="2" aria-label="Slide 3"></button>
                </div>
                <div className="carousel-inner">
                <div className="carousel-item active">
                    <img src="https://i.ytimg.com/vi/TMbEsPB0ib4/maxresdefault.jpg" className="d-block w-100" alt="Bleach Album Cover"></img>
                    <div className="carousel-caption d-none d-md-block">
                    <h5>Bleach</h5>
                    <p>
                        <i><b>Bleach</b></i> is the debut studio album by the America rock
                        band Nirvana, released on June 15, 1989, by 
                        <a href="https://en.wikipedia.org/wiki/Sub_Pop">
                            Sup Pop
                        </a>.
                    </p>
                    </div>
                </div>
                <div className="carousel-item">
                    <img src="https://wips.plug.it/cips/supereva/cms/2018/08/nirvana-nevermind.jpg" className="d-block w-100" alt="Nevermind Album Cover"></img>
                    <div className="carousel-caption d-none d-md-block">
                    <h5>Nevermind</h5>
                    <p>
                        <i><b>Nevermind</b></i> is the second studio album by the America rock
                        band Nirvana, released on September 24, 1991, by
                        <a href="https://en.wikipedia.org/wiki/DGC_Records">
                            DGC Records
                        </a>. 
                    </p>
                    </div>
                </div>
                <div className="carousel-item">
                    <img src={InUtero} className="d-block w-100" alt="In Utero Album Cover"></img>
                    <div className="carousel-caption d-none d-md-block">
                    <h5>In Utero</h5>
                    <p>
                        <i><b>In Utero</b></i> is the third studio album by the America rock
                        band Nirvana, released on September 21, 1993, by
                        <a href="https://en.wikipedia.org/wiki/DGC_Records">
                            DGC Records
                        </a>. 
                    </p>
                    </div>
                </div>
                </div>
                <button className="carousel-control-prev" type="button" data-bs-target="#carouselExampleCaptions" data-bs-slide="prev">
                <span className="carousel-control-prev-icon" aria-hidden="true"></span>
                <span className="visually-hidden">Previous</span>
                </button>
                <button className="carousel-control-next" type="button" data-bs-target="#carouselExampleCaptions" data-bs-slide="next">
                <span className="carousel-control-next-icon" aria-hidden="true"></span>
                <span className="visually-hidden">Next</span>
                </button>
            </div>
        </>
    )
}