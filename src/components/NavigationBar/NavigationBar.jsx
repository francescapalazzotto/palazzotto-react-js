// This component create a navbar in the header of the page 
export default function NavigationBar({home, name1, name2, name3, name4}){
    return(
        <>
         <nav className="navbar navbar-expand-lg bg-body-tertiary">
            <div className="container-fluid">
              <a className="navbar-brand" href="#">
                <img src="https://merchbar.imgix.net/product/105/6519/2547863846994/IByuL_Jn25a.png?q=40&auto=compress,format&w=1400" alt="Nirvana Logo Band" width="30" height="24" className="d-inline-block align-text-top"></img>
                NIRVANA</a>
              <button className="navbar-toggler" type="button" data-bs-toggle="collapse" data-bs-target="#navbarNavDropdown" aria-controls="navbarNavDropdown" aria-expanded="false" aria-label="Toggle navigation">
                <span className="navbar-toggler-icon"></span>
              </button>
              <div className="collapse navbar-collapse" id="navbarNavDropdown">
                <ul className="navbar-nav justify-content-end">
                  <li className="nav-item">
                    <a className="nav-link active" aria-current="page" href="#" style={{color: "yellow"}}>{home}</a>
                  </li>
                  <li className="nav-item">
                    <a className="nav-link" href="#band">{name1}</a>
                  </li>
                  <li className="nav-item">
                    <a className="nav-link" href="#tour">{name2}</a>
                  </li>
                  <li className="nav-item">
                    <a className="nav-link" href="#contact">{name3}</a>
                  </li>
                  <li className="nav-item dropdown">
                    <a className="nav-link dropdown-toggle" href="#" role="button" data-bs-toggle="dropdown" aria-expanded="false">
                      {name4}
                    </a>
                    <ul className="dropdown-menu">
                      <li><a className="dropdown-item" href="#">Action</a></li>
                      <li><a className="dropdown-item" href="#">Another action</a></li>
                      <li><a className="dropdown-item" href="#">Something else here</a></li>
                    </ul>
                  </li>
                </ul>
              </div>
            </div>
          </nav>
        </>
    )
}