export default function InfoTour(){
    return(
        <>
          <h1 id="tour" style={{color: "white"}}>TOUR DATES</h1>
          <p className="tour-dates-desc" style={{color: "white"}}>
              We'll play you some music. <br />
              Remember to book your tickets!            
          </p>
        </>
    )
}