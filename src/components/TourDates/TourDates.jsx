export default function TourDates(){
    return(
        <>
            <div className="container">

                <div className="row">
                    <div className="col-md-12">
                        <div className="card" >
                            <ul className="list-group list-group-flush">
                            <li className="list-group-item">April <span className="badge text-bg-danger">Sold Out!</span></li>
                            <li className="list-group-item">May <span className="badge text-bg-danger">Sold Out!</span></li>
                            <li className="list-group-item">June</li>
                            </ul>
                        </div>
                    </div>
                </div>

                <div className="row">
                    <div className="col-md-4">
                        <div className="card">
                            <img src="https://a.travel-assets.com/findyours-php/viewfinder/images/res70/171000/171732-Puget-Sound.jpg" className="card-img-top" alt="Tacoma City Image" />
                            <div className="card-body">
                            <h5 className="card-title">Tacoma</h5>
                            <p className="card-text" style={{fontStyle: "italic"}}>
                                June 12, 1987
                            </p>
                            <a href="https://www.ticketone.it/" className="btn btn-primary" style={{color: "white"}}>Buy tickets</a>
                            </div>
                        </div>
                    </div>

                    <div className="col-md-4">
                        <div className="card">
                            <img src="https://stateofwatourism.com/wp-content/uploads/2022/09/Olympia-City-Guide-9.jpg" className="card-img-top" alt="Olympia City Image" />
                            <div className="card-body">
                            <h5 className="card-title">Olympia</h5>
                            <p className="card-text" style={{fontStyle: "italic"}}>
                                June 27, 1987
                            </p>
                            <a href="https://www.ticketone.it/" className="btn btn-primary" style={{color: "white"}}>Buy tickets</a>
                            </div>
                        </div>
                    </div>

                    <div className="col-md-4">
                        <div className="card">
                            <img src="https://upload.wikimedia.org/wikipedia/commons/thumb/b/b3/Seattle_aerial_2%2C_May_2023.png/1024px-Seattle_aerial_2%2C_May_2023.png" className="card-img-top" alt="Seattle City Image" />
                            <div className="card-body">
                            <h5 className="card-title">Seattle</h5>
                            <p className="card-text" style={{fontStyle: "italic"}}>
                                June 30, 1987
                            </p>
                            <a href="https://www.ticketone.it/" className="btn btn-primary" style={{color: "white"}}>Buy tickets</a>
                            </div>
                        </div>
                    </div>

                </div>
            </div>
        </>
    )
}